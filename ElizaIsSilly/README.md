# Eliza Is Silly

#### Prerequisites:-

- NodeJS run time version >= 14.16
- Node module 'readline-sync'

### install 'readline-sync'

```
npm i readline-sync
```

### run the main.js

```
node main.js
```
