const Doctor=require("./doctor");
const readlineSync = require('readline-sync');
const prompt = require('prompt-sync')();

console.log(Doctor.intro());

let userInput = '';
while (userInput !=='quit') {
    userInput = readlineSync.question();
    let response = Doctor.response(userInput);
    console.log(response);
}