const BitcoinMiner = require('./BitcoinMiner');

playAgain = true
while (playAgain) {
  let game = new BitcoinMiner();
  game.play();
  playAgain = BitcoinMiner.getYesOrNo("Would you like to play again?");
}
console.log("Goodbye");