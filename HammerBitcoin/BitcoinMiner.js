const readline = require('readline-sync');

class BitcoinMiner {
    ANSI_RESET = "\u001B[0m";
    ANSI_RED = "\u001B[31m";
    ANSI_GREEN = "\u001B[32m";
    ANSI_YELLOW = "\u001B[33m";
    OGH = "O Great Gill Bates";


    #year = 1;
    #employees = 100;
    #cash = 2800;
    #computers = 1000;
    #computerPrice; //
    #starved = 0;
    #marketCrashVictims = 0;
    #newEmployees = 5;
    #cashMined = 3000;
    #bitcoinGeneratedPerComputer = 3;
    #amountStolenByHackers = 200;
    #cashPaidToEmployees;//
    #computersMaintained;//

    /**
     * Prints the introductory paragraph.
     */
    printIntroductoryParagraph(){
        console.log("Congratulations! You are the newest CEO of Make Me Rich, Inc, elected for a ten year term.");
        console.log("Your duties are to dispense living expenses for employees, direct mining of bitcoin, and");
        console.log("buy and sell computers as needed to support the corporation.");
        console.log("Watch out for hackers and market crashes!");
        console.log();
        console.log("Cash is the general currency, measured in bitcoins.");
        console.log();
        console.log("The following will help you in your decisions:");
        console.log("   * Each employee needs at least 20 bitcoins converted to cash per year to survive");
        console.log("   * Each employee can maintain at most 10 computers");
        console.log("   * It takes 2 bitcoins to pay for electricity to mine bitcoin on a computer.");
        console.log("   * The market price for computers fluctuates yearly");
        console.log();
        console.log("Lead the team wisely and you will be showered with appreciation at the end of your term.");
        console.log("Do it poorly and you will be terminated!");
    }
    /**
     * Allows the user to play the game.
     */
    play(){
        let stillInOffice = true;

        this.printIntroductoryParagraph();

        while (this.#year <= 10 && stillInOffice){
            this.#computerPrice = this.updateComputerPrice();
            this.printSummary();
            this.buyComputers();
            this.sellComputers();
            this.payEmployees();
            this.maintainComputers();

            this.#marketCrashVictims = this.checkForCrash();
            this.#employees =- this.#marketCrashVictims;

            if (this.countStarvedEmployees() >= 45){
                    stillInOffice = false;
            }

                this.#newEmployees = this.countNewHires();
                this.#employees += this.newEmployees;
                this.#cash += this.mineBitcoin(this.#computersMaintained);
                this.checkForHackers();
                this.#computerPrice = this.updateComputerPrice();
                this.#year =+ 1;
        }
        this.printFinalScore();
    }
    /**
    * Prints the year-end summary.
    */
    printSummary(){
         console.log(this.ANSI_YELLOW);
         console.log("___________________________________________________________________");
         console.log("\n" + this.OGH + "!");
         console.log(`You are in year ${this.#year} of your ten year rule.`);
         if (this.#marketCrashVictims > 0){
             console.log(`A terrible market crash wiped out ${this.#marketCrashVictims} of your team.`);
         }
         console.log(`In the previous year ${this.#starved} of your team starved to death.`);
         console.log(`In the previous year ${this.#newEmployees} employee(s) got employed by the corporation.`);
         console.log(`The employee head count is now: ${this.#employees}`);
         console.log(`We mined ${this.#cashMined} bitcoins at ${this.#bitcoinGeneratedPerComputer} bitcoins per computer.`);
         if (this.#amountStolenByHackers > 0){
             console.log(`*** Hackers stole ${this.#amountStolenByHackers} bitcoins, leaving ${this.#cash} bitcoins in your online wallet.`);
         }
         else{
             console.log(`We have ${this.#cash} bitcoins of cash in storage.`);
         }
         console.log(`The corporation owns ${this.#computers} computers for mining.`);
         console.log(`Computers currently cost ${this.#computerPrice} bitcoins each.`);
         console.log();
         console.log(this.ANSI_RESET);
     }

    /**
    * Allows the player to buy computers.
    *
    * If a valid amount is entered, the available cash is reduced accordingly.
    */
    buyComputers(){
        let question = "How many computers will you buy? ";
        let computersToBuy = this.getNumber(question);
        let cost = this.#computerPrice * computersToBuy;
        while (cost > this.#cash){
            this.jest(`We have but ${this.#cash} bitcoins of cash, not ${cost}!`);
            computersToBuy = getNumber(question);
            cost = this.#computerPrice * computersToBuy;
        }
        this.#cash = this.#cash - cost;
        this.#computers = this.#computers + computersToBuy;
        console.log(`${this.OGH}, you now have ${this.#computers} computers`);
        console.log(`and ${this.#cash} bitcoins of cash.`);
    }

    /**
    * Tells player that the request cannot be fulfilled.
    *
    * @param  message The reason the request cannot be fulfilled.
    */
     jest(message){
         console.log(`${this.OGH}, you are dreaming!`);
         console.log(message);
     }

    /**
    * Allows the player to sell computers.
    *
    * Available cash will be increased by the value of the computers sold.
    */
    sellComputers(){
        let question = "How many computers will you sell? ";
        let computersToSell = this.getNumber(question);

        while (computersToSell > this.#computers){
            this.jest(`The corporation has only ${this.#computers} computers!`);
            computersToSell = this.getNumber(question);
        }
        this.#cash = this.#cash + this.#computerPrice * computersToSell;
        this.#computers = this.#computers - computersToSell;
        console.log(`${this.OGH}, you now have ${this.#computers} computers`);
        console.log(`and ${this.#cash} bitcoins of cash.`);
    }

    /**
    * Allows the player to decide how much cash to use to feed people.
    *
    * If a valid amount is entered, the available cash is reduced accordingly.
    */
    payEmployees(){
        let question = "How much bitcoin will you distribute to the employees? ";
        this.#cashPaidToEmployees = this.getNumber(question);

        while (this.#cashPaidToEmployees > this.cash){
            this.jest(`We have but ${this.#cash} bitcoins!`);
            cashPaidToEmployees = this.getNumber(question);
        }
        this.#cash = this.#cash - this.#cashPaidToEmployees;
        console.log(`${this.OGH}, ${this.#cash} bitcoins remain.`);
    }

    /**
    * Allows the user to choose how much to spend on maintenance.
    */
     maintainComputers(){
        let question = "How many bitcoins will you allocate for maintenance? ";
        let maintenanceAmount = 0;
        let haveGoodAnswer = false;

        while (!haveGoodAnswer){
            maintenanceAmount = this.getNumber(question);
            if (maintenanceAmount > this.cash){
                this.jest(`We have but ${this.#cash} bitcoins left!`);
            }
            else if (maintenanceAmount > 2 * this.#computers){
                this.jest(`We have but ${this.#computers} computers available for mining!`);
            }
            else if (maintenanceAmount > 20 * this.#employees){
                this.jest(`We have but ${this.#employees} people to maintain the computers!`);
            }
            else{
                haveGoodAnswer = true;
            }
        }
        this.computersMaintained = maintenanceAmount / 2;
        // Be nice to the player!  If they enter an odd number, give them the extra bitcoin back.
        this.#cash = this.#cash - this.computersMaintained * 2;  // can re-write as cash -= computersMaintained * 2;
        console.log(`${this.OGH}, we now have ${this.#cash} bitcoins in storage.`);
     }

    /**
    * Checks for crash, and counts the victims.
    *
    * @return The number of victims of the crash.
    */
    checkForCrash() {
        let victims;
        if (Math.random() < 0.15){
            console.log("*** A terrible market crash wipes out half of the corporation's employees! ***");
            victims = this.#employees / 2;
        }
        else{
            victims = 0;
        }
        return victims;
    }

    /**
    * Counts how many people starved, and removes them from the employees.
    *
    * @return  The percent of employees who starved.
    */
    countStarvedEmployees(){
        let employeesPaid = this.#cashPaidToEmployees / 20;
        let percentStarved = 0;
        if (employeesPaid >= this.#employees){
            this.#starved = 0;
            console.log(this.ANSI_GREEN);
            console.log("The corporation's employees are well fed and happy.");
        }
        else{
            this.#starved = this.#employees - employeesPaid;
            console.log(this.ANSI_RED);
            console.log(`${this.#starved} employees starved to death.`);
            percentStarved = (100 * this.#starved) / this.#employees;
            this.#employees = this.#employees - this.#starved;
        }
        console.log(this.ANSI_RESET);
        return percentStarved;
    }

    /**
    * Counts how many new employees joined the company.
    *
    * @return The number of new hires.
    */
    countNewHires() {
        let newEmployees;
        if (this.#starved > 0){
            newEmployees = 0;
        }
        else{
            newEmployees = (20 * this.#computers + this.#cash) / (100 * this.#employees) + 1;
        }
        return newEmployees;
    }

    /**
    * Determines the harvest, and collects the new cash.
    *
    * Computers mine a random number of bitcoin each year, from 1 to 5.
    *
    * @return  The amount of bitcoin mined.
    */
    mineBitcoin(computers) {
        this.#bitcoinGeneratedPerComputer = Math.floor((Math.random() * 6) + 1);
        this.#cashMined = this.#bitcoinGeneratedPerComputer * this.#computers;
        return this.#cashMined;
    }

    /**
    * Checks if hackers get into the system, and determines how much they stole.
    */
    checkForHackers() {
        if (Math.floor((Math.random() * 100)) < 40){
            let percentHacked = 10 + Math.floor((Math.random() * 21));
            console.log(`*** Hackers steal  ${percentHacked} percent of your bitcoins! ***`);
            this.#amountStolenByHackers = (percentHacked * this.#cash) / 100;
            this.#cash = this.#cash - this.#amountStolenByHackers;
        }
        else{
            this.#amountStolenByHackers = 0;
        }
    }

    /**
    * Randomly sets the new price of computers.
    *
    * @return The new price of a computer.
    *
    * The price fluctuates from 17 to 26 bitcoin per computer.
    */
    updateComputerPrice() {
        return 17 + Math.floor((Math.random() * 10));
    }

    /**
    * Prints an evaluation at the end of a game.
    */
    printFinalScore() {

        if (this.#starved >= (45 * this.#employees) / 100){
            console.log(this.ANSI_RED);
            console.log(`O Once-Great Hammurabi,`);
            console.log(`${this.#starved} of your team starved during the last year of your incompetent reign!`);
            console.log("The few who remain hacked your bank account and changed your password, effectively evicting you!");
            console.log();
            console.log("Your final rating: TERRIBLE.");
            console.log(this.ANSI_RESET);
            return;
        }

        let computerScore = this.#computers;
        if (20 * this.#employees < computerScore){
            computerScore = 20 * this.#employees;
        }

        if (computerScore < 600){
            console.log(`Congratulations, ${this.OGH}`);
            console.log("You have ruled wisely but not well.");
            console.log("You have led your people through ten difficult years,");
            console.log(`but your corporations assets have shrunk to a mere ${this.#computers} computers.`);
            console.log();
            console.log("Your final rating: ADEQUATE.");
        }
        else if (computerScore < 800){
            console.log(this.ANSI_YELLOW);
            console.log(`Congratulations, ${this.OGH}.`);
            console.log("You  have ruled wisely, and shown the online world that its possible to make money in cryptocurrency.");
            console.log();
            console.log("Your final rating: GOOD.");
        }
        else{
            // console.ForegroundColor = consoleColor.Green;
            console.log(this.ANSI_GREEN);
            console.log(`Congratulations, ${this.OGH}`);
            console.log("You  have ruled wisely and well, and expanded your holdings while keeping your team happy.");
            console.log("Altogether, a most impressive job!");
            console.log();
            console.log("Your final rating: SUPERB.");
        }
        console.log(this.ANSI_RESET);

    }

    /**
    * Prints the given message (which should ask the user for some integral
    * quantity), and returns the number entered by the user. If the user's
    * response isn't an integer, the question is repeated until the user does
    * give an integer response.
    *
    * @param message
    *            The request to present to the user.
    * @return The user's numeric response.
    */
    getNumber(message){
        while (true)
        {
            console.log(message);
            var userInput = readline.question();
            try{
                return  parseInt(userInput);
            }
            catch (Exception){
                console.log(`${userInput} isn't a number!`);
            }
        }
    }
    /**
     * Returns a boolean response to a yes/no question.
     *
     * @param string
     *            The question to be asked.
     * @return True if the answer was yes, False if no.
     */
    static getYesOrNo(question){
        let answer;
        while (true) { // infinite loop.  return will exit the method, thus terminating the loop
            answer = readline.question(`${question}\r\n`);
            answer = answer.toLowerCase(answer);
            if (answer === 'y')
                return true;
            if (answer === 'n')
                return false;
        }
    }
}

module.exports = BitcoinMiner;